package com.example.medweb.service;

import com.example.medweb.model.Doktor;
import com.example.medweb.model.Pacient;

import java.util.List;
import java.util.Optional;

public interface DoktorService {
    List<Doktor> listAll();
    Optional<Doktor> findById(Integer id);
    Doktor login (String email, String pass);
}
