package com.example.medweb.service;

import com.example.medweb.model.Pacient;
import com.example.medweb.model.Upat;

import java.util.List;
import java.util.Optional;

public interface UpatService {

    Optional<Upat> findById (Integer id);
    List<Upat> findAllByPacient (Pacient pacient);
    Optional<Upat> selectedUpat (Integer upat_id);
}
