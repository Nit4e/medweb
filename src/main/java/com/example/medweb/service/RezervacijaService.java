package com.example.medweb.service;

import com.example.medweb.model.Rezervacija;

import java.util.Optional;

public interface RezervacijaService {

    void save (Rezervacija rezervacija);
    Optional<Rezervacija> findById (Integer id);
}
