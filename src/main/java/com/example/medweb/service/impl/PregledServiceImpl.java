package com.example.medweb.service.impl;

import com.example.medweb.model.Pregled;
import com.example.medweb.repository.PregledRepository;
import com.example.medweb.service.PregledService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PregledServiceImpl implements PregledService {

    private final PregledRepository pregledRepository;

    public PregledServiceImpl(PregledRepository pregledRepository) {
        this.pregledRepository = pregledRepository;
    }

    @Override
    public List<Pregled> findAll() {
        return this.pregledRepository.findAll();
    }
}
