package com.example.medweb.service.impl;

import com.example.medweb.model.Termin;
import com.example.medweb.model.TerminId;
import com.example.medweb.model.exceptions.TerminNotValidException;
import com.example.medweb.repository.TerminRepository;
import com.example.medweb.repository.TerminRepositoryCustom;
import com.example.medweb.service.TerminService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;


@Service
public class TerminServiceImpl implements TerminService {

    @Qualifier("main")
    private final TerminRepository terminRepository;
    @Qualifier(value = "custom")
    private final TerminRepositoryCustom terminRepositoryCustom;

    public TerminServiceImpl(TerminRepository terminRepository, TerminRepositoryCustom terminRepositoryCustom) {
        this.terminRepository = terminRepository;
        this.terminRepositoryCustom = terminRepositoryCustom;
    }

    @Override
    public Optional<Termin> findTerminById(TerminId terminId) {

        return this.terminRepository.findById(terminId);
    }

    @Override
    public List<Termin> findTerminByTermin_id(Integer termin_id) {

        return this.terminRepositoryCustom.findAllByTerminId(termin_id);
    }

    @Override
    public Termin findOneTerminByTerminId(Integer termin_id) {

        return this.terminRepositoryCustom.findByTerminId(termin_id);
    }

    @Override
    public void save(Termin termin) {
        if (termin.getVreme().isBefore(ZonedDateTime.now())) {
            throw new TerminNotValidException();
        }
        this.terminRepository.save(termin);
    }

    @Override
    public void deleteTermin(Termin termin) {

        this.terminRepository.delete(termin);
    }

    @Override
    public List<Termin> findOnlyFutureAndFree(ZonedDateTime now) {
        return this.terminRepository.findFutureAndFree(now);
    }

    @Override
    public List<Termin> findOnlyFutureAndFreeAndByDoktor(ZonedDateTime now, Integer doktor_id) {
        return this.terminRepository.findFutureAndFreeAndByDoktor(now, doktor_id);
    }
}
