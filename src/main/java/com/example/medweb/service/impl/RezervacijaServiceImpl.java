package com.example.medweb.service.impl;

import com.example.medweb.model.Rezervacija;
import com.example.medweb.repository.RezervacijaRepository;
import com.example.medweb.service.RezervacijaService;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class RezervacijaServiceImpl implements RezervacijaService {

    private final RezervacijaRepository rezervacijaRepository;

    public RezervacijaServiceImpl(RezervacijaRepository rezervacijaRepository) {
        this.rezervacijaRepository = rezervacijaRepository;
    }

    @Override
    public void save(Rezervacija rezervacija) {

        this.rezervacijaRepository.save(rezervacija);
    }

    @Override
    public Optional<Rezervacija> findById(Integer id) {
        return this.rezervacijaRepository.findById(id);
    }
}
