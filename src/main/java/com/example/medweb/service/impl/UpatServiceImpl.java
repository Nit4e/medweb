package com.example.medweb.service.impl;

import com.example.medweb.model.Pacient;
import com.example.medweb.model.Upat;
import com.example.medweb.model.exceptions.InvalidArgumentsException;
import com.example.medweb.repository.UpatRepository;
import com.example.medweb.service.UpatService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UpatServiceImpl implements UpatService {

    private final UpatRepository upatRepository;

    public UpatServiceImpl(UpatRepository upatRepository) {
        this.upatRepository = upatRepository;
    }

    @Override
    public Optional<Upat> findById(Integer id) {
        return this.upatRepository.findById(id);
    }

    @Override
    public List<Upat> findAllByPacient(Pacient pacient) {
        return this.upatRepository.findAllByPacient(pacient);
    }

    @Override
    public Optional<Upat> selectedUpat(Integer upat_id) {
        if (upat_id == null) {
            throw new InvalidArgumentsException();
        }
        return this.upatRepository.findById(upat_id);
    }
}
