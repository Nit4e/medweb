package com.example.medweb.service.impl;

import com.example.medweb.model.Doktor;
import com.example.medweb.model.exceptions.InvalidArgumentsException;
import com.example.medweb.model.exceptions.InvalidUserCredentialsException;
import com.example.medweb.repository.DoktorRepository;
import com.example.medweb.service.DoktorService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DoktorServiceImpl implements DoktorService {

    private final   DoktorRepository doktorRepository;

    public DoktorServiceImpl(DoktorRepository doktorRepository) {

        this.doktorRepository = doktorRepository;
    }
    @Override
    public List<Doktor> listAll() {

        return this.doktorRepository.findAll();
    }
    @Override
    public Optional<Doktor> findById(Integer id) {

        return this.doktorRepository.findById(id);
    }

    @Override
    public Doktor login(String email, String pass) {
        if (email == null || email.isEmpty() || pass == null || pass.isEmpty()) {
            throw new InvalidArgumentsException();
        }
        return doktorRepository.findDoktorByEmailAndPass(email, pass)
                .orElseThrow(InvalidUserCredentialsException::new);
    }
}
