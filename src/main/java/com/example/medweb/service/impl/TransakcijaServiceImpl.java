package com.example.medweb.service.impl;

import com.example.medweb.model.Transakcija;
import com.example.medweb.repository.TransakcijaRepository;
import com.example.medweb.service.TransakcijaService;
import org.springframework.stereotype.Service;


@Service
public class TransakcijaServiceImpl implements TransakcijaService {

    private final TransakcijaRepository transakcijaRepository;

    public TransakcijaServiceImpl(TransakcijaRepository transakcijaRepository) {
        this.transakcijaRepository = transakcijaRepository;
    }

    @Override
    public void save(Transakcija transakcija) {

        this.transakcijaRepository.save(transakcija);
    }
}
