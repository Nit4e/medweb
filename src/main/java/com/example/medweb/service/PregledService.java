package com.example.medweb.service;

import com.example.medweb.model.Pregled;

import java.util.List;

public interface PregledService {

    List<Pregled> findAll();
}
