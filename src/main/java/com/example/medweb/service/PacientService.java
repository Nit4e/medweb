package com.example.medweb.service;

import com.example.medweb.model.Pacient;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.Optional;

public interface PacientService {
    List<Pacient> listAll();
    Optional<Pacient> findById(Integer id);
    Optional<Pacient> findPacientByEmailAndPass(String email, String password);
    Optional<Pacient> findPacientByEmail(String email);
    void save(Pacient pacient);
    Pacient login (String email, String pass);
}
