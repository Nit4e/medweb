package com.example.medweb.web;

import com.example.medweb.model.Pacient;
import com.example.medweb.service.PacientService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Random;


@Controller
public class RegisterController {


    private final PacientService pacientService;

    public RegisterController(PacientService pacientService) {

        this.pacientService = pacientService;
    }

    // prikaz na templejtot za registracija
    @GetMapping ("/register")
    public String showRegisterForm(Pacient pacient, Model model){

        return "register.html";
    }

    // zachuvuvanje na noviot registriran pacient vo baza i prikaz na welcome stranata
    @PostMapping("/save")
    public String register(Pacient pacient, Model model){
        model.addAttribute("pacient", pacient);
        this.pacientService.save(pacient);
        return "welcome.html";
    }
}

