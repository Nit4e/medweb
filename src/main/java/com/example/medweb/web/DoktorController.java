package com.example.medweb.web;


import com.example.medweb.model.Doktor;
import com.example.medweb.model.Termin;
import com.example.medweb.model.exceptions.InvalidUserCredentialsException;
import com.example.medweb.service.DoktorService;
import com.example.medweb.service.TerminService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class DoktorController {

    private final DoktorService doktorService;
    private final TerminService terminService;

    public DoktorController(DoktorService doktorService, TerminService terminService) {
        this.doktorService = doktorService;
        this.terminService = terminService;
    }

    // prikazhi forma za login na doktor
    @GetMapping("/login-doktor")
    public String showLoginForm(Model model){
        model.addAttribute("doktor", new Doktor());
        model.addAttribute("bodyContent", new Doktor());
        model.addAttribute("bodyContent", "login-doktor");
        return "master-template";
    }

    // najava na doktor i redirekcija do pochetna ako e uspeshen loginot
    // najaveniot doktor se zachuvuva vo sesija
    @PostMapping("/login-doktor")
    public String loginDoktor (HttpServletRequest request, Model model) {
        Doktor doktor = null;

        try  {
            doktor = this.doktorService.login(request.getParameter("email"),
                    request.getParameter("pass"));
            request.getSession().setAttribute("doktor", doktor);
            return "redirect:/home-doktor";
        }
        catch (InvalidUserCredentialsException exception) {
            model.addAttribute("hasError", true);
            model.addAttribute("error", exception.getMessage());
            return "login-doktor";
        }
    }

    // pochetna strana na doktor posle uspeshen login
    @GetMapping("/home-doktor")
    public String getDoktorHomePage (Model model) {
        model.addAttribute("bodyContent", "home-doktor");
        return "master-template";
    }

    // pregled na site termini objaveni od najaveniot doktor (kopche moi termini)
    @GetMapping("/moi-termini")
    public String getTermini (HttpServletRequest request, Doktor doktor, Model model) {
        doktor = (Doktor) request.getSession().getAttribute("doktor");
        //List<Termin> terminList = doktor.getTerminList();
        List<Termin> terminList = this.terminService
                .findOnlyFutureAndFreeAndByDoktor(ZonedDateTime.now(), doktor.getCovek_id());
        model.addAttribute("terminList", terminList);
        model.addAttribute("bodyContent", terminList);
        model.addAttribute("bodyContent", "doktor-termini");
        return "master-template";
    }

    // prikaz na templejt za vnes na nov termin
    @GetMapping("/vnesi-termin/{id}")
    public String getEditTerminPage (@PathVariable Integer id, Model model) {
        Doktor doktor = this.doktorService.findById(id).get();
        model.addAttribute("termin", new Termin());
        model.addAttribute("bodyContent", new Termin());
        model.addAttribute("bodyContent", "vnesi-nov-termin");
        return "master-template";
    }

    // posle vnesen datum i chas za terminot, istiot se vnesuva vo bazata
    // prikaz na podatocite za uspeshno objaveniot termin
    @PostMapping("/save-termin")
    public String saveTermin (Model model, HttpServletRequest request,
                              @RequestParam(value = "vreme") String vreme){
        Doktor doktor = (Doktor) request.getSession().getAttribute("doktor");
        try {
            Termin termin = new Termin();
            termin.setDoktor_id(doktor.getCovek_id());
            ZoneId zoneId = ZoneId.systemDefault();
            ZonedDateTime vremeZ = LocalDateTime.parse(vreme, DateTimeFormatter.ISO_DATE_TIME).atZone(zoneId);
            termin.setVreme(vremeZ);
            model.addAttribute("termin", termin);
            model.addAttribute("bodyContent", termin);
            model.addAttribute("bodyContent", "nov-termin");
            this.terminService.save(termin);
            return "master-template";
        } catch (RuntimeException exception) {
            return "redirect:/moi-termini?error=" + exception.getMessage();
        }

    }

    // brishenje na termin
    @GetMapping("/termin/{id}/delete")
    @Transactional
    public String delete(@PathVariable Integer id) {
        if (this.terminService.findOneTerminByTerminId(id) != null) {
            Termin termin = this.terminService.findOneTerminByTerminId(id);
            this.terminService.deleteTermin(termin);
            return "redirect:/moi-termini";
        }
        else {
            return "redirect:/home-doktor";
        }
    }
}
