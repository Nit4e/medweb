package com.example.medweb.web;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class LogoutController {

    @GetMapping("/logout")
    public String logout (HttpServletRequest request) {
        request.getSession().invalidate();
        return "index";
    }
}
