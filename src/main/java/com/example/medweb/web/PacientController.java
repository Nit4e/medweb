package com.example.medweb.web;


import com.example.medweb.model.*;
import com.example.medweb.model.exceptions.InvalidUserCredentialsException;
import com.example.medweb.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.time.ZonedDateTime;
import java.util.List;



@Controller
@SessionAttributes({"upat", "termin", "rezervacija"})
public class PacientController {

    private final PacientService pacientService;
    private final UpatService upatService;
    private final DoktorService doktorService;
    private final TerminService terminService;
    private final TransakcijaService transakcijaService;
    private final RezervacijaService rezervacijaService;

    public PacientController(PacientService pacientService, UpatService upatService, DoktorService doktorService,
                             TerminService terminService, TransakcijaService transakcijaService,
                             RezervacijaService rezervacijaService) {

        this.pacientService = pacientService;
        this.upatService = upatService;
        this.doktorService = doktorService;
        this.terminService = terminService;
        this.transakcijaService = transakcijaService;
        this.rezervacijaService = rezervacijaService;
    }

    // prikazhi forma za login na pacient
    @GetMapping("/login-pacient")
    public String showLoginForm(Model model){
        model.addAttribute("pacient", new Pacient());
        model.addAttribute("bodyContent", new Pacient());
        model.addAttribute("bodyContent", "login-pacient");
        return "master-template";
    }

    // najava na pacient i redirekcija do pochetna ako e uspeshen loginot
    // najaveniot pacient se zachuvuva vo sesija
    @PostMapping("/login-pacient")
    public String loginPacient (@Validated @ModelAttribute ("pacient") Pacient pacient,
                                HttpServletRequest request, Model model) {
        //Pacient pacient = null;

        try  {
            pacient = this.pacientService.login(request.getParameter("email"),
                    request.getParameter("pass"));
            request.getSession().setAttribute("pacient", pacient);
            model.addAttribute("pacient", pacient);
            return "redirect:/home-pacient";
        }
        catch (InvalidUserCredentialsException exception) {
            model.addAttribute("hasError", true);
            model.addAttribute("error", exception.getMessage());
            return "login-pacient";
        }
    }

    // pochetna strana na pacient posle uspeshen login
    @GetMapping("/home-pacient")
    public String getPacientHomePage (Model model) {
        model.addAttribute("bodyContent", "home-pacient");
        return "master-template";
    }

    // lista na izdadeni upati na pacient (kopche moi upati)
    @GetMapping("/moi-upati")
    public String getUpati (HttpServletRequest request, Pacient pacient, Model model) {
        pacient = (Pacient) request.getSession().getAttribute("pacient");
        List<Upat> upatList = this.upatService.findAllByPacient(pacient);
        model.addAttribute("upatList", upatList);
        model.addAttribute("bodyContent", upatList);
        model.addAttribute("bodyContent", "pacient-upati");
        return "master-template";
    }

    // posle klikanje na kopcheto 'zakazhi termin' do nekoj upat,
    // za toj upat se prikazhuvaat speijalistite koi rabotat vo soodvetniot oddel,
    // do sekoj specijalist ima kopche 'pregledaj termini'
    @GetMapping("/zakazi-termin")
    public String getZakaziTerminPage (Model model) {
        model.addAttribute("upat", new Upat());
        model.addAttribute("bodyContent", new Upat());
        model.addAttribute("bodyContent", "zakazi-termin");
        return "master-template";
    }

    // posle klikanje na kopcheto 'zakazhi termin' do nekoj upat,
    // za toj upat se prikazhuvaat speijalistite koi rabotat vo soodvetniot oddel,
    // do sekoj specijalist ima kopche 'pregledaj termini'
    @GetMapping("/zakazi-termin/{id}")
    public String zakaziTermin (@PathVariable Integer id, Model model) {
        if (this.upatService.findById(id).isPresent()) {
            Upat upat = this.upatService.findById(id).get();
            String dijagnoza = upat.getDijagnoza();
            Integer oddelId = upat.getOddel().getOddel_id();
            String oddel = upat.getOddel().getNaziv();
            List<Doktor> doktorList = upat.getOddel().getDoktorList();
            model.addAttribute("upat", upat);
            model.addAttribute("bodyContent", upat);
            model.addAttribute("dijagnoza", dijagnoza);
            model.addAttribute("bodyContent", dijagnoza);
            model.addAttribute("oddelId", oddelId);
            model.addAttribute("bodyContent", oddelId);
            model.addAttribute("oddel", oddel);
            model.addAttribute("bodyContent", oddel);
            model.addAttribute("doktorList", doktorList);
            model.addAttribute("bodyContent", doktorList);
            model.addAttribute("bodyContent", "zakazi-termin");
            return "master-template";
        }
        else {
            return "redirect:/zakazi-termin/{id}?error=UpatNotFound";
        }
    }

    // so izbiranje na specijalist od listata na doktori, se prikazhuvaat terminite za istiot,
    // do sekoj termin ima akcija 'zakazhi'
    @GetMapping("/dostapni-termini/{id}")
    public String getDostapniTerminiPage (@PathVariable Integer id, Model model) {
        Upat upat = (Upat) model.getAttribute("upat");
        if (this.doktorService.findById(id).isPresent()) {
            Doktor doktor = this.doktorService.findById(id).get();
            List<Termin> terminList = this.terminService
                    .findOnlyFutureAndFreeAndByDoktor(ZonedDateTime.now(), doktor.getCovek_id());
            model.addAttribute("doktor", doktor);
            model.addAttribute("bodyContent", doktor);
            model.addAttribute("terminList", terminList);
            model.addAttribute("bodyContent", terminList);
            model.addAttribute("bodyContent", "dostapni-termini");
            return "master-template";
        }
        else {
            return "redirect:/dostapni-termini/{id}?error=UpatNotFound";
        }
    }

    // so izbiranje na termin se dodava nov zapis vo rezervacija
    // se prikazhuva strana na koja treba da se sledat chekorite za potvrda na rezervacija
    @PostMapping("/rezervacija/{termin_id}")
    public String rezervacija (@PathVariable Integer termin_id, Model model){
        Upat upat = (Upat) model.getAttribute("upat");
        if (this.terminService.findOneTerminByTerminId(termin_id) != null) {
            Termin termin = this.terminService.findOneTerminByTerminId(termin_id);
            model.addAttribute("termin", termin);
            Rezervacija rezervacija = new Rezervacija();
            rezervacija.setUpat(upat);
            rezervacija.setTermin(termin);
            this.rezervacijaService.save(rezervacija);
            model.addAttribute("rezervacija", rezervacija);
            model.addAttribute("bodyContent", rezervacija);
            model.addAttribute("bodyContent", "rezervacija");
            return "master-template";
        }
        else {
            return "redirect:/dostapni-termini/{id}?error=TerminNotFound";
        }
    }

    // prikaz na formata za transakcija za da se validira rezerviraniot termin
    @GetMapping("/validiraj/rezervacija/{id}")
    public String getTransakcijaPage (@PathVariable Integer id, Model model) {
        Upat upat = (Upat) model.getAttribute("upat");
        if (this.rezervacijaService.findById(id).isPresent()) {
            Rezervacija rezervacija = this.rezervacijaService.findById(id).get();
            model.addAttribute("bodyContent", "transakcija");
            return "master-template";
        }
        else {
            return "redirect:/validiraj/rezervacija/{id}?error=RezervacijaNotFound";
        }
    }

    // so uspeshno popolneti parametri se dodava transakcijata vo baza
    // i se prikazhuva strana so site podatoci deka e uspeshno zakazhan terminot
    @PostMapping("/validiraj/rezervacija/{id}")
    public String transakcija (@PathVariable Integer id, Model model) {
        Upat upat = (Upat) model.getAttribute("upat");
        Termin termin = (Termin) model.getAttribute("termin");
        if (this.rezervacijaService.findById(id).isPresent()) {
            Rezervacija rezervacija = this.rezervacijaService.findById(id).get();
            String smetka_bolnica = upat.getOddel().getBolnica_id().getSmetka_bolnica();
            Integer suma = 50;
            Transakcija transakcija = new Transakcija(suma, smetka_bolnica, rezervacija);
            this.transakcijaService.save(transakcija);
            model.addAttribute("transakcija", transakcija);
            model.addAttribute("bodyContent", transakcija);
            model.addAttribute("bodyContent", "prikaz-rezervacija");
            return "master-template";
        }
        else {
            return "redirect:/validiraj/rezervacija/{id}?error=RezervacijaNotFound";
        }
    }
}
