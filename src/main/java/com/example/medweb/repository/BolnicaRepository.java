package com.example.medweb.repository;

import com.example.medweb.model.Bolnica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BolnicaRepository extends JpaRepository<Bolnica, Long> {
}
