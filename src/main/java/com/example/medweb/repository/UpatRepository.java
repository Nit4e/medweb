package com.example.medweb.repository;

import com.example.medweb.model.Pacient;
import com.example.medweb.model.Upat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UpatRepository extends JpaRepository<Upat, Integer> {

    Optional<Upat> findById(Integer upat_id);

    @Query("select u from Upat u where u.pacient = :pacient")
    List<Upat> findAllByPacient (Pacient pacient);
}
