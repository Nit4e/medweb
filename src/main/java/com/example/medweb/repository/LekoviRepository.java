package com.example.medweb.repository;

import com.example.medweb.model.Lekovi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LekoviRepository extends JpaRepository<Lekovi, Long> {
}
