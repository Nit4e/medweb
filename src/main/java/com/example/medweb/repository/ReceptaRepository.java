package com.example.medweb.repository;

import com.example.medweb.model.Recepta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ReceptaRepository extends JpaRepository<Recepta, Integer> {
}
