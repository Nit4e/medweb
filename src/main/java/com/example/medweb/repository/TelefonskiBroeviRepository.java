package com.example.medweb.repository;

import com.example.medweb.model.TelefonskiBroevi;
import com.example.medweb.model.TelefonskiBroeviId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TelefonskiBroeviRepository extends JpaRepository<TelefonskiBroevi, TelefonskiBroeviId> {
}
