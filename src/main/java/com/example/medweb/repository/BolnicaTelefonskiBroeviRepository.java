package com.example.medweb.repository;

import com.example.medweb.model.BolnicaBroeviId;
import com.example.medweb.model.BolnicaTelefonskiBroevi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BolnicaTelefonskiBroeviRepository extends JpaRepository<BolnicaTelefonskiBroevi, BolnicaBroeviId> {
}
