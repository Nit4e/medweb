package com.example.medweb.repository;

import com.example.medweb.model.Oddel;
import com.example.medweb.model.OddelId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface OddelRepository extends JpaRepository<Oddel, OddelId> {
}
