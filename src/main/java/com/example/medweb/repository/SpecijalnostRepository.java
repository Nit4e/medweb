package com.example.medweb.repository;

import com.example.medweb.model.Specijalnost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface SpecijalnostRepository extends JpaRepository<Specijalnost, Integer> {
}
