package com.example.medweb.repository;

import com.example.medweb.model.Doktor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface DoktorRepository extends JpaRepository<Doktor, Integer> {

    Optional<Doktor> findDoktorByEmailAndPass(String email, String pass);

    Optional<Doktor> findById(Integer id);
}
