package com.example.medweb.repository;


import com.example.medweb.model.Pacient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface PacientRepository extends JpaRepository<Pacient, Integer> {

    Optional<Pacient> findPacientByEmailAndPass(String email, String pass);
    Optional<Pacient> findPacientByEmail(String email);
}
