package com.example.medweb.repository;

import com.example.medweb.model.Rezervacija;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RezervacijaRepository extends JpaRepository<Rezervacija, Integer> {
}
