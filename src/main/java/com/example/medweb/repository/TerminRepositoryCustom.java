package com.example.medweb.repository;

import com.example.medweb.model.Termin;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TerminRepositoryCustom {

    List<Termin> findAllByTerminId (Integer termin_id);
    Termin findByTerminId (Integer termin_id);
    void deleteByTerminId (Integer termin_id);
}
