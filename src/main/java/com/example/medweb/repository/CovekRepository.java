package com.example.medweb.repository;

import com.example.medweb.model.Covek;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface CovekRepository extends JpaRepository<Covek, Long> {

    Optional<Covek> findCovekByEmailAndPass(String email, String password);
    Optional<Covek> findCovekByEmail(String email);
}
