package com.example.medweb.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Data
@Entity
public class Lekovi {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer lek_id;
    private String ime_lek;
    private String genericko_ime;

    public Lekovi() {
    }

    public Lekovi(String ime_lek, String genericko_ime) {
        this.ime_lek = ime_lek;
        this.genericko_ime = genericko_ime;
    }
}
