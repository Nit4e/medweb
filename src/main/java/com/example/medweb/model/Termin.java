package com.example.medweb.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;


@Data
@Entity
@IdClass(TerminId.class)
@Table(name = "termin")
public class Termin implements Serializable {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "termin_id", insertable = false, updatable = false)
    private Integer termin_id;

    @Id
    private Integer doktor_id;

    @Id
    @ManyToOne
    @JoinColumn(name = "doktor_id", referencedColumnName = "doktor_id", insertable = false, updatable = false)
    private Doktor doktor;

    private ZonedDateTime vreme;

    public Termin() {
    }
}
