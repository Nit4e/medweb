package com.example.medweb.model;


import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Data
@Entity
@PrimaryKeyJoinColumn(name = "doktor_id")
public class Doktor extends Covek implements Serializable {

    private String opis;
    private Integer br_licenca;



    @ManyToOne
    @JoinColumn(name = "spec_id", referencedColumnName = "spec_id", nullable = true)
    private Specijalnost specijalnost;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "bolnica_id", referencedColumnName = "bolnica_id", nullable = false),
            @JoinColumn(name = "oddel_id", referencedColumnName = "oddel_id", nullable = false)
    })
    private Oddel oddel;

    @OneToMany(mappedBy = "doktor", fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Termin> terminList;

    @OneToMany(mappedBy = "doktor", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Pregled> pregledList;

    public Doktor () {}

    public Doktor(String email, String pass, String embg, String ime, String prezime, String opis,
                  Integer br_licenca, Specijalnost specijalnost, Oddel oddel, List<Termin> terminList,
                  List<Pregled> pregledList) {
        super(email, pass, embg, ime, prezime);
        this.opis = opis;
        this.br_licenca = br_licenca;
        this.specijalnost = specijalnost;
        this.oddel = oddel;
        this.terminList = terminList;
        this.pregledList = pregledList;
    }
}
