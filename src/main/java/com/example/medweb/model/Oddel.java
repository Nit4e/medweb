package com.example.medweb.model;


import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
@IdClass(OddelId.class)
public class Oddel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer oddel_id;

    @ManyToOne
    @JoinColumn(name = "bolnica_id", referencedColumnName = "bolnica_id", nullable = false)
    @Id
    private Bolnica bolnica_id;

    private String naziv;

    @ManyToOne
    @JoinColumn(name = "spec_id", referencedColumnName = "spec_id", nullable = true)
    private Specijalnost specijalnost;

    @OneToMany(mappedBy = "oddel", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Doktor> doktorList;

    public Oddel() {
    }

    public Oddel(String naziv, Specijalnost specijalnost, List<Doktor> doktorList) {
        this.naziv = naziv;
        this.specijalnost = specijalnost;
        this.doktorList = doktorList;
    }
}
