package com.example.medweb.model;



import lombok.Data;

import javax.persistence.*;


@Data
@Entity
public class Upat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer upat_id;
    private String dijagnoza;

    @ManyToOne
    @JoinColumn(name = "covek_pacient_id", referencedColumnName = "covek_pacient_id", nullable = false)
    private Pacient pacient;

    @ManyToOne
    @JoinColumn(name = "pregled_id", referencedColumnName = "pregled_id", nullable = true)
    private Pregled pregled;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "bolnica_id", referencedColumnName = "bolnica_id", nullable = false),
            @JoinColumn(name = "oddel_id", referencedColumnName = "oddel_id", nullable = false)
    })
    private Oddel oddel;

    @OneToOne(mappedBy = "upat")
    private Rezervacija rezervacija;

    public Upat() {
    }

    public Upat(String dijagnoza) {
        this.dijagnoza = dijagnoza;
    }

    public Upat(Long id) {
    }
}
