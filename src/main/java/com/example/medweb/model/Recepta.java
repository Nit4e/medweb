package com.example.medweb.model;

import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;


@Data
@Entity
public class Recepta {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer recepta_id;
    private ZonedDateTime datum_izdavanje;
    private ZonedDateTime datum_vaznost;
    private ZonedDateTime datum_kupuvanje;

    @ManyToOne
    @JoinColumn(name = "doktor_id", referencedColumnName = "covek_id", nullable = false)
    Doktor doktor;

    @ManyToOne
    @JoinColumn(name = "covek_pacient_id", referencedColumnName = "covek_id", nullable = false)
    private Pacient pacient;

    @ManyToOne
    @JoinColumn(name = "lek_id", referencedColumnName = "lek_id", nullable = false)
    Lekovi lek;

    @ManyToOne
    @JoinColumn(name = "pregled_id", referencedColumnName = "pregled_id", nullable = true)
    private Pregled pregled;


    public Recepta() {
    }

    public Recepta(ZonedDateTime datum_izdavanje, ZonedDateTime datum_vaznost, ZonedDateTime datum_kupuvanje) {
        this.datum_izdavanje = datum_izdavanje;
        this.datum_vaznost = datum_vaznost;
        this.datum_kupuvanje = datum_kupuvanje;
    }
}