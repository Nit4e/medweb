package com.example.medweb.model;


import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;


@Data
@Entity
public class Pregled {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer pregled_id;
    private ZonedDateTime vreme ;

    @ManyToOne
    @JoinColumn(name = "doktor_id", referencedColumnName = "doktor_id", nullable = true)
    Doktor doktor;

    @ManyToOne
    @JoinColumn(name = "covek_pacient_id", referencedColumnName = "covek_pacient_id", nullable = false)
    private Pacient pacient;


    public Pregled() {
    }

    public Pregled(ZonedDateTime vreme) {
        this.vreme = vreme;
    }

    public Pregled(Pregled pregled) {
    }

    public Pregled(Doktor doktor, Long pregled_id) {
    }
}
