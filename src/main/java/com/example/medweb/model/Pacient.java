package com.example.medweb.model;


import lombok.Data;
import org.hibernate.annotations.*;


import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;


@Data
@Entity
@PrimaryKeyJoinColumn(name = "covek_pacient_id")
public class Pacient extends Covek implements Serializable {

    @Column(nullable = true)
    private Integer pacient_id;

    @OneToMany(mappedBy = "pacient", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Upat> upatList;

    public Pacient() {
    }

    public Pacient(String email, String pass, String embg, String ime, String prezime, Integer pacient_id,
                   List<Upat> upatList) {
        super(email, pass, embg, ime, prezime);
        this.pacient_id = pacient_id;
        this.upatList = upatList;
    }

    public Pacient(String ime, String prezime, String embg, String email, String encryptPass) {

    }

}
