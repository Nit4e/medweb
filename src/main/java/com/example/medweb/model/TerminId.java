package com.example.medweb.model;


import lombok.Data;

import java.io.Serializable;
import java.util.Objects;


@Data
public class TerminId implements Serializable {

    private Integer doktor_id;
    private Integer termin_id;

    public TerminId() {
    }

    public TerminId(Integer doktor_id, Integer termin_id) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TerminId terminId = (TerminId) o;
        return doktor_id.equals(terminId.doktor_id) &&
                termin_id.equals(terminId.termin_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(doktor_id, termin_id);
    }
}
