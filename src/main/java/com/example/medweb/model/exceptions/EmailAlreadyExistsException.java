package com.example.medweb.model.exceptions;

public class EmailAlreadyExistsException extends RuntimeException {

    public EmailAlreadyExistsException(String email) {

        super(String.format("Email %s already Eexists", email));
    }
}
