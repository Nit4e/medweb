package com.example.medweb.model.exceptions;

public class TerminNotValidException extends RuntimeException {

    public TerminNotValidException() {
        super("Ne mozhete da vnesete termin vo minatoto!");
    }
}
