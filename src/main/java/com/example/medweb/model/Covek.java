package com.example.medweb.model;


import lombok.Data;

import javax.persistence.*;



@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "email")})
public class Covek {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer covek_id;
    @Column(unique = true)
    private String email;
    private String pass;
    @Column(columnDefinition = "bpchar(13)")
    private String embg;
    private String ime;
    private String prezime;

    public Covek() {
    }

    public Covek(String email, String pass, String embg, String ime, String prezime) {
        this.email = email;
        this.pass = pass;
        this.embg = embg;
        this.ime = ime;
        this.prezime = prezime;
    }
}
