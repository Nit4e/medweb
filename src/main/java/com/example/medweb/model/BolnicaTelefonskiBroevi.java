package com.example.medweb.model;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;


@Data
@Entity
@IdClass(BolnicaBroeviId.class)
public class BolnicaTelefonskiBroevi {


    @Id
    private Integer bolnica_id;

    @Id
    private String telefonski_br_bolnica ;

    public BolnicaTelefonskiBroevi() {
    }
}
